 

import { Component, OnInit } from '@angular/core';

import { interval } from 'rxjs';

import * as url from 'url';

 

@Component({

  selector: 'app-screen-saver',

  templateUrl: './screen-saver.component.html',

  styleUrls: ['./screen-saver.component.scss'],

})

export class ScreenSaverComponent implements OnInit {

  constructor() {}

  imageUrls = ['https://picsum.photos/1920/1080'];

  // count = 0;

 

  ngOnInit(): void {

    const period = interval(5000);

    period.subscribe((value) => {

      this.imageUrls.push('https://picsum.photos/1920/1080?' + value);

      console.log(value);

      // this.count = value;

    });

  }

}
