import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ScreenSaverComponent } from './screen-saver/screen-saver.component';

@NgModule({
  declarations: [
    AppComponent,
    ScreenSaverComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
